nathanael sebastien

# Évaluation Git
 Exercices


1 point - Fourcher et cloner le dépôt
**git clone git@gitlab.com:nathanaelsbtn/git-exercise.git**

1 point - Remplir votre nom ci-dessus

**ok**


1 point - Reprendre dans master le commit de la branche equality_refinement via cherry-pick

** git cherry-pick fbca3b12**


1 point - Défaire le commit remove unused subtract functions

**git revert 01877329**


1 point - Fusionner dans master la branche doc/intro

**git merge 12af7e2d**


2 points - Fusionner dans master la branche add_distance
**git merge 071d365a**


1 point - Pousser le nouveau master


**git add -Av**
**git commit -m "Fusion doc/intro"**
**git push -u origin master**



2 points - Créer une branche sur le dernier master, créer une nouvelle fonction de calcul, la committer, et pousser cette nouvelle branche
**git checkout -b "fonc_calc"**
**function addition**
**git add -Av**
**git commit -m "Fonc_addi"**
**git push -u origin "fonc_calc"**


2 points - Rebaser la branche add_tests sur master
**git rebase master**


1 point - Pousser la branche add_tests sur le dépôt distant
**git checkout -b "add_tests"**
**git add -Av**
**git commit -m "rebase add_tests"**
**git push -u origin master**


2 points - Ouvrir une merge request à partir de cette branche add_tests sur le dépôt source
git merge request branc "add_tests"


## Questions


Donner l’auteur du commit qui a introduit la fonction add dans ce projet : 

**Père Noël**


Indiquer quelle commande utiliser, dans un projet Git,  pour changer le message du dernier commit : 

**git commit --amend -m "new_msg"**


Quelle commande, inverse de git add, permet de retirer un fichier du stage (prochain commit) ? :

** git reset**


Donner le nom du créateur de Git : 
**Jalil Arfaoui**


Indiquer la(les) différence(s) entre les commandes git init et git clone : 

**git init initialisation d'un nouveau dossier**

**git clone un dossier existant en local sur visual code**